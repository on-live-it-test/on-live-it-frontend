import { defineConfig, HttpProxy, ProxyOptions } from "vite";
import react from "@vitejs/plugin-react-swc";

const logOutgoingRequests = (targetName: string) => {
    return (proxy: HttpProxy.Server, _options: ProxyOptions) => {
        proxy.on("error", (err, _req, _res) => {
            console.log(`${targetName} error`, err);
        });
        proxy.on("proxyReq", (proxyReq, req, _res) => {
            console.log(`Sending Request to ${targetName}:`, req.method, req.url);
        });
        proxy.on("proxyRes", (proxyRes, req, _res) => {
            console.log(`Received Response from ${targetName}:`, proxyRes.statusCode, req.url);
        });
    };
};

// https://vitejs.dev/config/
export default defineConfig({
    base: "./",
    plugins: [react()],
    server: {
        port: 8081,
        proxy: {
            "/local-backend": {
                target: "http://localhost:3000/api",
                changeOrigin: true,
                rewrite: (path) => path.replace("/local-backend", ""),
                secure: false,
                configure: logOutgoingRequests("local-backend"),
            },
            "/on-live-it": {
                target: "https://onlive-backend.onrender.com/api",
                changeOrigin: true,
                rewrite: (path) => path.replace("/on-live-it", ""),
                secure: false,
                configure: logOutgoingRequests("on-live-it-backend"),
            },
            "/public-local-backend": {
                target: "http://localhost:3000/public",
                changeOrigin: true,
                rewrite: (path) => path.replace("/public-local-backend", ""),
                secure: false,
                configure: logOutgoingRequests("local-backend"),
            },
            "/public-on-live-it": {
                target: "https://onlive-backend.onrender.com/public",
                changeOrigin: true,
                rewrite: (path) => path.replace("/public-on-live-it", ""),
                secure: false,
                configure: logOutgoingRequests("on-live-it-backend"),
            },
        },
    },
});
