## Installation

```
npm i
```

## Start

```
npm start
```

## environment (.env.local)

if you want to use local server

```
VITE_APP_LOCAL_BACKEND=boolean
```

## deployment

Pushing to the main branch will trigger deployment
