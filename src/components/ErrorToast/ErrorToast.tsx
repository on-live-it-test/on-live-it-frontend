import { useEffect, useState } from "react";
import { Toast, ToastContainer } from "react-bootstrap";
import { useAtom } from "jotai";
import { errorMessageAtom } from "../../store/toast";

export const ErrorToast = () => {
    const [errorMessage, setErrorMessage] = useAtom(errorMessageAtom);

    return (
        <ToastContainer position="bottom-center" className="p-3">
            <Toast bg="warning" show={!!errorMessage} delay={4000} autohide onClose={() => setErrorMessage("")}>
                <Toast.Body className="d-flex justify-content-center">{errorMessage}</Toast.Body>
            </Toast>
        </ToastContainer>
    );
};
