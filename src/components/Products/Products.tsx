import { useEffect, useMemo } from "react";
import { Card, Placeholder } from "react-bootstrap";
import { useAtom } from "jotai";

import { useProducts } from "../../api/products";
import { Product } from "./Product/Product";
import styles from "./Products.module.css";
import card from "../../assets/card_placeholder.svg";
import { errorMessageAtom } from "../../store/toast";

export const Products = () => {
    const [products, loading, errorMessage] = useProducts();
    const [_, setErrorMessage] = useAtom(errorMessageAtom);

    useEffect(() => {
        if (errorMessage) setErrorMessage(errorMessage);
    }, [errorMessage]);

    const productList = useMemo(
        () => products.map(({ productId, title, cost }) => <Product key={productId} title={title} cost={cost} productId={productId} />),
        [products]
    );

    return (
        <div className="mt-md-5">
            <div className={styles.product_container}>
                {loading ? (
                    <>
                        <Card style={{ width: "18rem" }}>
                            <Card.Img height={180} width={288} variant="top" src={card} />
                            <Card.Body>
                                <Placeholder as={Card.Title} animation="glow">
                                    <Placeholder xs={6} />
                                </Placeholder>
                                <Placeholder as={Card.Text} animation="glow">
                                    <Placeholder xs={7} /> <Placeholder xs={4} /> <Placeholder xs={4} /> <Placeholder xs={6} /> <Placeholder xs={8} />
                                </Placeholder>
                                <Placeholder.Button variant="primary" xs={6} />
                            </Card.Body>
                        </Card>

                        <Card style={{ width: "18rem" }}>
                            <Card.Img variant="top" src={card} />
                            <Card.Body>
                                <Placeholder as={Card.Title} animation="glow">
                                    <Placeholder xs={6} />
                                </Placeholder>
                                <Placeholder as={Card.Text} animation="glow">
                                    <Placeholder xs={7} /> <Placeholder xs={4} /> <Placeholder xs={4} /> <Placeholder xs={6} /> <Placeholder xs={8} />
                                </Placeholder>
                                <Placeholder.Button variant="primary" xs={6} />
                            </Card.Body>
                        </Card>

                        <Card style={{ width: "18rem" }}>
                            <Card.Img variant="top" src={card} />
                            <Card.Body>
                                <Placeholder as={Card.Title} animation="glow">
                                    <Placeholder xs={6} />
                                </Placeholder>
                                <Placeholder as={Card.Text} animation="glow">
                                    <Placeholder xs={7} /> <Placeholder xs={4} /> <Placeholder xs={4} /> <Placeholder xs={6} /> <Placeholder xs={8} />
                                </Placeholder>
                                <Placeholder.Button variant="primary" xs={6} />
                            </Card.Body>
                        </Card>

                        <Card style={{ width: "18rem" }}>
                            <Card.Img variant="top" src={card} />
                            <Card.Body>
                                <Placeholder as={Card.Title} animation="glow">
                                    <Placeholder xs={6} />
                                </Placeholder>
                                <Placeholder as={Card.Text} animation="glow">
                                    <Placeholder xs={7} /> <Placeholder xs={4} /> <Placeholder xs={4} /> <Placeholder xs={6} /> <Placeholder xs={8} />
                                </Placeholder>
                                <Placeholder.Button variant="primary" xs={6} />
                            </Card.Body>
                        </Card>
                    </>
                ) : (
                    productList
                )}
            </div>
        </div>
    );
};
