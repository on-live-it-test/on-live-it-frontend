import { Button, Card } from "react-bootstrap";
import { baseUrl } from "../../../api/baseUrl";

interface PropTypes {
    cost: number;
    title: string;
    productId: string;
}

export const Product = (props: PropTypes) => {
    return (
        <Card style={{ width: "18rem" }}>
            <Card.Img variant="top" src={`${baseUrl()}/products/images/${props.productId}`} />
            <Card.Body>
                <Card.Title>{props.title}</Card.Title>
                <Card.Text>Cost: ${props.cost}</Card.Text>
                <Button variant="primary align-self-end">Next</Button>
            </Card.Body>
        </Card>
    );
};
