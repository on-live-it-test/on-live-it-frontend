import { Link } from "react-router-dom";
import { Navbar, Container, Nav } from "react-bootstrap";

export const Header = () => {
    return (
        <Navbar bg="primary" variant="dark">
            <Container>
                <Navbar.Brand>Hatházi Marcell: +36302825988</Navbar.Brand>
                <Nav className="me-auto">
                    <Nav.Link href="/" className="p-x-5">
                        HOME
                    </Nav.Link>
                    <Nav.Link href="/login" className="p-x-5">
                        LOGIN
                    </Nav.Link>
                    <Nav.Link href="/statistics" className="p-x-5">
                        STATISTICS
                    </Nav.Link>
                </Nav>
            </Container>
        </Navbar>
    );
};
