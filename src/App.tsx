import { BrowserRouter, Route, Routes } from "react-router-dom";
import Container from "react-bootstrap/esm/Container";
import { useAtom } from "jotai";

import { Home } from "./pages/Home";
import { Login } from "./pages/Login";
import { Product } from "./pages/Product";
import { Header } from "./components/Header/Header";

import "./App.css";
import { accessToken } from "./store/token";
import { ErrorToast } from "./components/ErrorToast/ErrorToast";
import { Unauthorized } from "./pages/Unauthorized";
import { Statistics } from "./pages/Statistics";

function App() {
    const [token] = useAtom(accessToken);

    return (
        <>
            <Header />
            <ErrorToast />
            <Container>
                <BrowserRouter>
                    <Routes>
                        <Route path="/" element={<Home />} />
                        <Route path="/login" element={<Login />} />
                        <Route path="/statistics" element={<Statistics />} />
                        {<Route path="/product" element={token ? <Product /> : <Unauthorized />} />}
                        <Route path="*" element={<p>There's nothing here: 404!</p>} />
                    </Routes>
                </BrowserRouter>
            </Container>
        </>
    );
}

export default App;
