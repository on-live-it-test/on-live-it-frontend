import { Button, Card, Container, FloatingLabel, Form } from "react-bootstrap";
import { Formik } from "formik";
import * as yup from "yup";
import { useState } from "react";
import { postProduct, uploadProductImage } from "../api/product";
import { useNavigate } from "react-router-dom";
const productInit = {
    title: "",
    cost: "",
    active: false,
};

type ProductT = typeof productInit;

const productSchema = yup.object({
    title: yup.string().required(),
    cost: yup.number().min(0).required(),
    active: yup.bool().required(),
});

export const Product = () => {
    const navigate = useNavigate();
    const [file, setFile] = useState<File>();

    const handleForm = async (value: ProductT) => {
        try {
            // TODO: handle file missing error in this if
            if (!file) return;

            const response = await postProduct({
                title: value.title,
                cost: +value.cost,
                active: value.active,
            });

            await uploadProductImage(file, response.productId);
            navigate("/");
        } catch (error) {}
    };

    const handleFileUpload = (file: HTMLInputElement) => {
        if (!file) return;
        const files = file.files as FileList;
        if (files.length < 1) return;
        const selectedFile = files[files.length - 1];
        setFile(selectedFile);
    };

    return (
        <Container className="mt-5 d-flex justify-content-center">
            <Card className="p-4 rounded-0" style={{ width: "25rem" }}>
                <Card.Title style={{ fontSize: "36px" }} className="mb-5 pl-2">
                    Add Product
                </Card.Title>
                <Formik validationSchema={productSchema} onSubmit={handleForm} initialValues={productInit}>
                    {({ handleSubmit, handleChange, values, touched, errors, isValid, setFieldValue }) => (
                        <Form noValidate onSubmit={handleSubmit}>
                            <FloatingLabel controlId="validationFormikEmail" label="Title" className="position-relative">
                                <Form.Control
                                    type="text"
                                    name="title"
                                    autoFocus
                                    placeholder="Title"
                                    value={values.title}
                                    onChange={handleChange}
                                    isInvalid={touched.title && !!errors.title}
                                    isValid={touched.title && !errors.title}
                                />
                            </FloatingLabel>
                            <FloatingLabel controlId="validationFormikCost" label="Cost" className="position-relative mt-3">
                                <Form.Control
                                    type="number"
                                    name="cost"
                                    placeholder="Cost"
                                    value={values.cost}
                                    onChange={handleChange}
                                    isInvalid={touched.cost && !!errors.cost}
                                    isValid={touched.cost && !errors.cost}
                                />
                            </FloatingLabel>

                            <Form.Group className="position-relative mt-3">
                                <Form.Control
                                    name="img"
                                    type="file"
                                    onChange={(event) => {
                                        handleFileUpload(event.currentTarget as HTMLInputElement);
                                        handleChange(event);
                                    }}
                                    accept=".jpg, .png, .svg"
                                    size="lg"
                                />
                            </Form.Group>

                            <Form.Group controlId="validationFormikActive" className="mt-3">
                                <Form.Check
                                    checked={values.active}
                                    onChange={(e) => setFieldValue("active", e.target.checked)}
                                    type="checkbox"
                                    label="Active"
                                />
                            </Form.Group>

                            <div className="d-grid gap-2 mt-4">
                                <Button disabled={!isValid || !file} className="mt-5 btn-block" type="submit" size="lg">
                                    Send
                                </Button>
                            </div>
                        </Form>
                    )}
                </Formik>
            </Card>
        </Container>
    );
};
