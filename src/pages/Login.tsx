import { useEffect, useState } from "react";
import { Button, Card, Container, FloatingLabel, Form } from "react-bootstrap";
import { Formik } from "formik";
import * as yup from "yup";
import { useNavigate } from "react-router-dom";
import { useAtom } from "jotai/react";

import { login } from "../api/auth";
import { accessToken, accessTokenExpiresIn } from "../store/token";
import { errorMessageAtom } from "../store/toast";

const capitalLatterRegExp = new RegExp("(?=.*?[A-Z])");
const letterRegExp = new RegExp("(?=(.*[a-z]))");
const numberRegExp = new RegExp("(?=.*[0-9])");
const emailRegExp = new RegExp(
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i
);

const loginInit = {
    email: "",
    password: "",
};

type loginT = typeof loginInit;

const loginSchema = yup.object({
    email: yup.string().matches(emailRegExp).required(),
    password: yup.string().min(8).max(16).matches(capitalLatterRegExp).matches(letterRegExp).matches(numberRegExp).required(),
});

export const Login = () => {
    const navigate = useNavigate();
    const [error, setError] = useState(null);
    const [token, setToken] = useAtom(accessToken);
    const [expiresIn, setExpiresIn] = useAtom(accessTokenExpiresIn);
    const [errorMessage, setErrorMessage] = useAtom(errorMessageAtom);

    const handleForm = async (values: loginT) => {
        try {
            const response = await login(values);

            sessionStorage.setItem("access_token", response.token);
            setToken(response.token);
            setExpiresIn(response.expiresIn);
            navigate("/product");
        } catch (error: any) {
            setError(error.message);
        }
    };

    useEffect(() => {
        if (error) {
            setErrorMessage(error);
        }
    }, [error]);

    return (
        <Container className="mt-5 d-flex justify-content-center">
            <Card className="p-4 rounded-0" style={{ width: "25rem" }}>
                <Card.Title style={{ fontSize: "36px" }} className="mb-5 pl-2">
                    Please login
                </Card.Title>
                <Formik validationSchema={loginSchema} onSubmit={handleForm} initialValues={loginInit}>
                    {({ handleSubmit, handleChange, values, touched, errors, isValid }) => (
                        <Form noValidate onSubmit={handleSubmit}>
                            <FloatingLabel controlId="validationFormikEmail" label="Email Address" className="position-relative">
                                <Form.Control
                                    className="rounded-top rounded-0"
                                    type="email"
                                    name="email"
                                    autoFocus
                                    placeholder="Email Address"
                                    value={values.email}
                                    onChange={handleChange}
                                    isInvalid={touched.email && !!errors.email}
                                    isValid={touched.email && !errors.email}
                                />
                            </FloatingLabel>
                            <FloatingLabel controlId="validationFormikPassword" label="Password" className="position-relative">
                                <Form.Control
                                    className="rounded-bottom rounded-0"
                                    type="password"
                                    name="password"
                                    placeholder="Password"
                                    value={values.password}
                                    onChange={handleChange}
                                    isInvalid={touched.password && !!errors.password}
                                    isValid={touched.password && !errors.password}
                                />
                            </FloatingLabel>

                            <div className="d-grid gap-2 mt-4">
                                <Button disabled={!isValid} className="mt-5 btn-block" type="submit" size="lg">
                                    Login
                                </Button>
                            </div>
                        </Form>
                    )}
                </Formik>
            </Card>
        </Container>
    );
};
