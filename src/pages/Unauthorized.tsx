import { useEffect } from "react";
import { useAtom } from "jotai";
import { errorMessageAtom } from "../store/toast";
import { useNavigate } from "react-router-dom";

export const Unauthorized = () => {
    const [_, setErrorMessage] = useAtom(errorMessageAtom);
    const navigate = useNavigate();

    useEffect(() => {
        setErrorMessage("Unauthorized! Log in first.");

        navigate("/login");
    }, []);

    return null;
};
