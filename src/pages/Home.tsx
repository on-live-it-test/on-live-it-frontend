import { Products } from "../components/Products/Products";

export const Home = () => {
    return (
        <div>
            <Products />
        </div>
    );
};
