import { Container, Spinner } from "react-bootstrap";
import { BarChart, Bar, XAxis, YAxis, Tooltip, CartesianGrid } from "recharts";

import { useProducts } from "../api/products";

export const Statistics = () => {
    const [products, loading, productsErrors] = useProducts();

    return (
        <Container style={{ minHeight: 400 }} className="d-flex justify-content-center align-items-center">
            {loading ? (
                <Spinner />
            ) : (
                <BarChart width={600} height={300} data={products} className="align-self-end">
                    <XAxis dataKey="title" stroke="#8884d8" />
                    <YAxis />
                    <Tooltip />
                    <CartesianGrid stroke="#ccc" strokeDasharray="5 5" />
                    <Bar dataKey="cost" fill="#8884d8" barSize={30} />
                </BarChart>
            )}
        </Container>
    );
};
