import { atom } from "jotai";

export const accessToken = atom<string>("");
// TODO: don't forget to implement refresh token
export const refreshToken = atom<string>("");
export const accessTokenExpiresIn = atom<number>(0);
