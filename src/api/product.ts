import { mutate, uploadFile } from "./fetch";

interface IProduct {
    title: string;
    cost: number;
    active: boolean;
}

type IPostProductResponse = Promise<{
    productId: string;
}>;

export const postProduct = async (product: IProduct): IPostProductResponse => {
    const response = await mutate("/products/add", product);
    return await response.json();
};

export const uploadProductImage = async (file: File, productId: string) => {
    const response = await uploadFile(`/products/images/upload/${productId}`, file, productId);

    return await response.json();
};
