import { mutate } from "./fetch";

interface ILoginData {
    email: string;
    password: string;
}

interface LoginResponse {
    token: string;
    expiresIn: number;
}

export const login = async (payload: ILoginData): Promise<LoginResponse> => {
    const response = await mutate("/auth/login", payload);
    return await response.json();
};
