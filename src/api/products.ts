import { useEffect, useState } from "react";
import { get } from "./fetch";

interface IProduct {
    productId: string;
    title: string;
    cost: number;
}

export const useProducts = (deps: any[] = []): [IProduct[], boolean, string | null] => {
    const [products, setProducts] = useState<IProduct[]>([]);
    const [loading, setLoading] = useState(false);
    const [productsErrors, setProductsErrors] = useState(null);

    const getProducts = async () => {
        try {
            setLoading(true);
            const response = await get(`/products`);

            const { products } = await response.json();

            setProducts(products);
            setLoading(false);
        } catch (error: any) {
            setLoading(false);
            setProductsErrors(error.message);
        }
    };

    useEffect(() => {
        getProducts();
    }, deps);

    return [products, loading, productsErrors];
};
