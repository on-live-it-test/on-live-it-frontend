import { baseUrl } from "./baseUrl";

type Method = "POST" | "PATCH" | "GET" | "DELETE" | "PUT";

export const mutate = (url: string, data: object, method: Method = "POST") => {
    const access_token = sessionStorage.getItem("access_token") ?? "";
    const header: HeadersInit = {};

    if (access_token) {
        header["Authorization"] = access_token;
    }

    header["Content-type"] = "application/json";

    return fetch(`${baseUrl()}${url}`, {
        method,
        body: JSON.stringify(data),
        headers: header,
    });
};

export const get = (url: string) => {
    return fetch(`${baseUrl()}${url}`);
};

export const uploadFile = (url: string, file: File, fileName: string) => {
    const access_token = sessionStorage.getItem("access_token") ?? "";
    const formData = new FormData();

    formData.append("file", file, fileName);
    return fetch(`${baseUrl()}${url}`, {
        method: "POST",
        body: formData, // This is your file object
        headers: {
            Authorization: access_token,
        },
    });
};
