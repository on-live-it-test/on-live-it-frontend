const local = (import.meta.env.VITE_APP_LOCAL_BACKEND as string) === "true";
let baseUrlReached = false;

(async () => {
    if (local) {
        const res = await fetch(`${local ? "local-backend" : "on-live-it"}/`);
        if (res.status === 200) baseUrlReached = true;
    }
})();

export const baseUrl = () =>
    import.meta.env.PROD ? "https://onlive-backend.onrender.com/api" : local ? (baseUrlReached ? "local-backend" : "on-live-it") : "on-live-it";
